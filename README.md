# Krip Interview Test Project

This project is solely made for the assessment test at Krip. The main task is to built a scraper to scrape a website and convert it into structured data that contains certain keys, that's all.

## Keep it simple

Based on the requirement of the assessment, the project output sould contains 3 keys, as follow:
- [x] Merchant
- [x] Credit Card
- [x] Rewards

So, here I create a simple CLI app that scrape the web page and return the fetched data into a JSON, whether as a file or just as CLI output. The main reason is of course to make it easy to integrate with another app or make this tool as background services that run via cron job or supervisor, or even scale it up even more later on.

At glance, the offers shown on the page has multi-level filters that can be adjusted as needed. Say, We want to see is there any offer for different type of cards (Premier MasterCard, Advance Visa Platinum Card, etc) or for different category. We could simply pu them as CLI options.

### Current Implementation

The code is fairly simple, at least for now. It just scrape the page then parse the given information about available offers and return it as a JSON. We can add a bunch of features later on since the base app is already done, like store the data to database, or parse/format the output for better data structure, etc.

## Usage

First you need to install the dependencies with the following command

```shell
$> yarn install # or npm install
```

By default this project is using `Yarn`, but feel free to use `NPM` if you wish. Now you can run the code with the following command

```shell
$> node cli.mjs scan
```

The code will start scanning the page which is by default is page for _All HSBC credit cards_, _Dinning_ spending category and _Red Hot Happy Days Rewards_ subcategory.

You can change the default filters by passing additional parameter to the CLI like so, or run the following command to see all available options

```shell
$> node cli.mjs scan -h
Usage: krip-test scan [options]

scan the offers

Options:
  -c, --card <card>   Select card type (default: "Others")
  --spending <spend>  Select spending category (default: "Dinning")
  --sub <sub>         Select sub category (default: "Red Hot Happy Days Rewards")
  -h, --help          display help for command
```

Available `--card` filters are

- **Premier** for _Premier MasterCard_
- **Advance** for _Advance Visa Platinum Card_
- **Platinum** for _Platinum and above_
- **Others** for _All HSBC credit cards_

Available `--spending` filters are

- Dinning
- Shopping
- Travel & Lifestyle
- Healt Care
- Online
- Merchant Installment
- Greater Bay Are

Available `--sub` filters are

- Red Hot Happy Days Rewards
- Hotel Dining
- Chinese Cuisine
- Western Cuisine
- Japanese Cuisine
- Korean & Asian Cuisine
- Hotpot
- Celebratory Events - Hotel
- Celebratory Events - Others
- Maxim's Dining Offers
- Online & others

**NOTE:** You should wrap the filter with single-quote or double-quote for any filter that have space. Example:

```shell
$> node cli.mjs scan --card Platinum --spending 'Merchant Installment' --sub 'Online & others'
```

Once the code done scanning the page, you'll see output like these

```js
[
  {
    id: 'Y2021-CATALOHEALTH-S01-000-paa',
    logoURL: 'https://www.redhotoffers.hsbc.com.hk/media/21704014/S1-0034.jpg',
    merchant: 'CATALO Naturals (China) Limited',
    detail: '12% off health food supplements\n' +
      '5% off healthy snacks and drink mix powder\n' +
      'www.catalo.com'
  },
  {
    id: 'Y2021-CHECKCHECKIN-S03-000-paa',
    logoURL: 'https://www.redhotoffers.hsbc.com.hk/media/26414298/S3-0079.jpg',
    merchant: 'CheckCheckCin',
    detail: '10% off Paper Pack Rice Water\nwww.checkcheckcin.com'
  },
  ...
]
```
