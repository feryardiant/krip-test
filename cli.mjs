#!/usr/bin/env node

import { createCommand } from 'commander/esm.mjs'
import inquirer from 'inquirer'
import { readJSON, cardTypes, spendingCategories, subCategories } from './util.mjs'
import { scanOffers } from './main.mjs';

const pkg = readJSON('package.json')
const program = createCommand()
  .name(pkg.name)
  .version(pkg.version, '-V, --version', 'output the program version')

const defaultCardType = 'Others'
const defaultSpendingCat = 'Dinning'
const defaultSubCat = 'Red Hot Happy Days Rewards'

program
  .command('scan')
  .description('scan the offers')
  .option(
    '-c, --card <card>',
    'select card type',
    defaultCardType
  )
  .option(
    '--spending <spend>',
    'select spending category',
    defaultSpendingCat
  )
  .option(
    '--sub <sub>',
    'select sub category',
    defaultSubCat
  )
  .action(async (opts) => {
    const { cardType, spendingCat, subCat } = await inquirer.prompt([
      {
        type: 'list',
        name: 'cardType',
        message: 'Please select card type',
        default: defaultCardType,
        when: !(opts.card in cardTypes),
        choices: Object.keys(cardTypes)
      },
      {
        type: 'list',
        name: 'spendingCat',
        message: 'Please select spending category',
        default: defaultSpendingCat,
        when: !spendingCategories.includes(opts.spending),
        choices: spendingCategories
      },
      {
        type: 'list',
        name: 'subCat',
        message: 'Please select sub category',
        default: defaultSubCat,
        when: !subCategories.includes(opts.sub),
        choices: subCategories
      }
    ])

    try {
      const result = await scanOffers(
        cardType || opts.card,
        spendingCat || opts.spending,
        subCat || opts.sub
      )

      console.log(result)
    } catch (e) {
      console.error(e)
    }
  })

;(async function main() {
  await program.parseAsync(process.argv)
})()
