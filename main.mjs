import axios from 'axios'
import cheerio from 'cheerio'

const BASEURL = 'https://www.redhotoffers.hsbc.com.hk/'

function parseHtml(html) {
  const result = {
    offers: [],
    pages: []
  }

  if (!html) return result

  const $ = cheerio.load(html)

  /**
   * @param {import('cheerio').Cheerio} $elm
   * @returns {String}
   */
  const normalizeText = ($elm) => {
    const text = $('p', $elm).text().replace(/\t/g, '') +
      $('.expandDetails', $elm).text().replace(/\t/g, '')

    return text.split(/\n/)
      .filter((line) => !!line)
      .join('\n')
  }

  $('.yroContent > center a').each((_, $link) => {
    result.pages.push(Number($($link).text()))
  })

  $('.yro-promo-block').each((_, $offer) => {
    const $img = $('.logo img', $offer).first()
    const offer = {
      id: $('a.anchor', $offer).first().attr('id').slice(9),
      logoURL: new URL($img.attr('src'), BASEURL).toString(),
      merchant: $img.attr('alt'),
      detail: normalizeText($('.col-md-9', $offer))
    }

    result.offers.push(offer)
  })

  return result
}

async function scrape(params) {
  const fetch = axios.create({
    baseURL: BASEURL,
    headers: {
      'Referer': BASEURL,
      'Connection': 'keep-alive',
      'Upgrade-Insecure-Requests': 1,
      'Accept-Language': 'en-US,en;q=0.9',
      'Accept-Encoding': 'gzip, deflate',
      'Cookie': 'over18=18',
      'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    },
    responseType: 'document'
  })

  const { data, status } = await fetch('/en/yro', {
    params,
  })

  return [200, 302].includes(status) ? data : null
}

export async function scanOffers(cardType = 'Others', spendingCat = null, subCat = null) {
  let page = 1
  let finished = false
  const results = []

  while (!finished) {
    const html = await scrape({ cardType, spendingCat, subCat, page })
    const { offers, pages } = parseHtml(html)

    results.push(...offers)
    page++

    finished = !pages.includes(page)
  }

  return results
}
