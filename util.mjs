import { dirname, resolve } from 'path'
import { existsSync, readFileSync } from 'fs'

const isWin = process.platform === 'win32'
const __dirname = dirname(import.meta.url.slice(isWin ? 8 : 6))
const fileOption = {
  encoding: 'utf8',
}

export const cardTypes = {
  Premier: 'Premier MasterCard',
  Advance: 'Advance Visa Platinum Card',
  Platinum: 'Platinum and above',
  Others: 'All HSBC credit cards',
}

export const spendingCategories = [
  'Dinning',
  'Shopping',
  'Travel & Lifestyle',
  'Healt Care',
  'Online',
  'Merchant Installment',
  'Greater Bay Area'
]

export const subCategories = [
  'Red Hot Happy Days Rewards',
  'Hotel Dining',
  'Chinese Cuisine',
  'Western Cuisine',
  'Japanese Cuisine',
  'Korean & Asian Cuisine',
  'Hotpot',
  'Celebratory Events - Hotel',
  'Celebratory Events - Others',
  'Maxim\'s Dining Offers',
  'Online & others',
]

/**
 * @param {String} filename
 * @returns {Object}
 */
export function readJSON(filename) {
  if (!existsSync(resolve(__dirname, filename))) {
    return {}
  }

  const fileContent = readFileSync(resolve(__dirname, filename), fileOption)

  return fileContent ? JSON.parse(fileContent) : null
}
